<div class="alert alert-danger" role="alert">
    <ul>
    <?php foreach ($errors as $key =>  $error) : ?>
        <li>[<?=$key?>] <?= esc($error) ?></li>
    <?php endforeach ?>
    </ul>
</div>