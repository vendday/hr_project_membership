<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Register | 24Hrs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="Themesbrand" name="author">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url('admin/assets/images/favicon.ico') ?>">
    <!-- Bootstrap Css -->
    <link href="<?= base_url('admin/assets/css/bootstrap.min.css') ?>" id="bootstrap-style" rel="stylesheet" type="text/css">
    <!-- Icons Css -->
    <link href="<?= base_url('admin/assets/css/icons.min.css') ?>" rel="stylesheet" type="text/css">
    <!-- App Css-->
    <link href="<?= base_url('admin/assets/css/app.min.css') ?>" id="app-style" rel="stylesheet" type="text/css">

</head>

<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <!-- Begin page -->
    <div class="accountbg" style="background: url('../admin/assets/images/hr-bg.jpg');background-size: cover;background-position: center;"></div>

    <div class="account-pages mt-5 pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center mt-4">
                                <div class="mb-3">
                                    <a href="index.html"><img src="<?= base_url('admin/assets/images/hr-logo.png') ?>" height="30" alt="logo"></a>
                                </div>
                            </div>
                            <div class="p-3">
                                <h4 class="font-size-18 mt-2 text-center">Your account has been activated.</h4>
                                <p class="text-muted text-center mb-4">We will redirect you to login page in <span id="counter"><?php echo "00:{$input[0]}"; ?></span>.</p>

                            </div>

                        </div>
                    </div>
                    <div class="mt-5 text-center position-relative">
                        <p class="text-white">Already have an account ? <a href="/login" class="font-weight-bold text-primary"> Login </a> </p>
                        <p class="text-white">2021 © 24Hrs.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <!-- JAVASCRIPT -->
    <script src="<?= base_url('admin/assets/libs/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/metismenu/metisMenu.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/simplebar/simplebar.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/node-waves/waves.min.js') ?>"></script>

    <script src="<?= base_url('../admin/assets/js/app.js') ?>"></script>





    <script>
        $(document).ready(function() {
            var duration = <?php echo $input[0]; ?>,
                display = document.querySelector('#counter');
            startTimer(duration, display);


            function startTimer(duration, display) {
                var timer = duration,
                    minutes, seconds;
                setInterval(function() {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    display.textContent = minutes + ":" + seconds;

                    if (--timer < 0) {
                        // timer = duration;
                        window.location = "<?php echo $input[1]; ?>";
                    }
                }, 1000);
            }
        });
    </script>

</body>

</html>