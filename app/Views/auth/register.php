<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Register | 24Hrs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="Themesbrand" name="author">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url('admin/assets/images/favicon.ico') ?>">

    <!-- Bootstrap Css -->
    <link href="<?= base_url('admin/assets/css/bootstrap.min.css') ?>" id="bootstrap-style" rel="stylesheet" type="text/css">
    <!-- Icons Css -->
    <link href="<?= base_url('admin/assets/css/icons.min.css') ?>" rel="stylesheet" type="text/css">
    <!-- App Css-->
    <link href="<?= base_url('admin/assets/css/app.min.css') ?>" id="app-style" rel="stylesheet" type="text/css">

</head>

<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <!-- Begin page -->
    <div class="accountbg" style="background: url('../admin/assets/images/hr-bg.jpg');background-size: cover;background-position: center;"></div>

    <div class="account-pages mt-5 pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center mt-4">
                                <div class="mb-3">
                                    <a href="index.html"><img src="<?= base_url('admin/assets/images/hr-logo.png') ?>" height="30" alt="logo"></a>
                                </div>
                            </div>
                            <div class="p-3">
                                <!-- <h4 class="font-size-18 mt-2 text-center">Free Register</h4> -->
                                <p class="text-muted text-center mb-4">Get your 24HRs account now.</p>


                                <?php if (isset($validation) && $validation->getErrors()) : ?>
                                    <?= $validation->listErrors('my_list'); ?>
                                <?php endif; ?>

                                <form class="form-horizontal" method="post" action="sign-up" autocomplete="off">

                                    <input type="hidden" class="form-control" id="setup" name="setup" value="">

                                    <div class="mb-3">
                                        <label class="form-label" for="account">Account Name</label>
                                        <input type="text" class="form-control" id="account" name="account" placeholder="Enter account name. Can be change later." value="">
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label" for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="">
                                    </div>

                                    <!-- <div class="mb-3">
                                            <label class="form-label" for="username">Username</label>
                                            <input type="text" class="form-control" id="username" placeholder="Enter username">
                                        </div> -->

                                    <div class="mb-3">
                                        <label class="form-label" for="password">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" value="" autocomplete="new-password">
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label" for="password_confirm">Confirm Password</label>
                                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm password" value="" autocomplete="new-password">
                                    </div>

                                    <div class="mb-3">
                                        <div class="text-end">
                                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Register</button>
                                        </div>
                                    </div>

                                    <div class="mb-0 row">
                                        <div class="col-12 mt-4">
                                            <p class=" text-muted mb-0">By registering you agree to the 24HRs <a href="#">Terms of Use</a></p>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                    <div class="mt-5 text-center position-relative">
                        <p class="text-white">Already have an account ? <a href="/login" class="font-weight-bold text-primary"> Login </a> </p>
                        <p class="text-white">2021 © 24Hrs.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <!-- JAVASCRIPT -->
    <script src="<?= base_url('admin/assets/libs/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/metismenu/metisMenu.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/simplebar/simplebar.min.js') ?>"></script>
    <script src="<?= base_url('admin/assets/libs/node-waves/waves.min.js') ?>"></script>

    <script src="<?= base_url('../admin/assets/js/app.js') ?>"></script>

</body>

</html>