
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        2021 © 24Hrs.
                    </div>
                </div>
            </div>
        </footer>
        
        <div id="confirmDeleteModal" class="modal fade" tabindex="-1" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="confirmDeleteModalLabel"><?=lang('Validation.confirm_delete') ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <?= lang('Validation.confirm_delete_msg') ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal" aria-label="Close"><?= lang('Validation.cancel') ?></button>
                        <a class="btn btn-primary waves-effect waves-light btn-ok"><?= lang('Validation.confirm') ?></a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- JAVASCRIPT -->
        <script>
            $('#confirmDeleteModal').on('show.bs.modal', function(e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        </script>

        <!-- <script src="<?= base_url('admin/assets/libs/jquery/jquery.min.js')?>"></script> -->
        <script src="<?= base_url('admin/assets/libs/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/metismenu/metisMenu.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/simplebar/simplebar.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/node-waves/waves.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js')?>"></script>

        <!-- Required datatable js -->
        <script src="<?= base_url('admin/assets/libs/datatables.net/js/jquery.dataTables.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')?>"></script>

        <!-- Buttons examples -->
        <script src="<?= base_url('admin/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/jszip/jszip.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/pdfmake/build/pdfmake.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/pdfmake/build/vfs_fonts.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/datatables.net-buttons/js/buttons.html5.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/datatables.net-buttons/js/buttons.print.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')?>"></script>

        <!-- Responsive examples -->
        <script src="<?= base_url('admin/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')?>"></script>

        <!-- Datatable init js -->
        <script src="<?= base_url('admin/assets/js/pages/datatables.init.js')?>"></script> 

        <!-- Peity chart-->
        <script src="<?= base_url('admin/assets/libs/peity/jquery.peity.min.js')?>"></script>
        
        <!--C3 Chart-->
        <script src="<?= base_url('admin/assets/libs/d3/d3.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/libs/c3/c3.min.js')?>"></script>
        
        <script src="<?= base_url('admin/assets/libs/jquery-knob/jquery.knob.min.js')?>"></script>
        
        <script src="<?= base_url('admin/assets/js/pages/dashboard.init.js')?>"></script>
        
        <script src="<?= base_url('admin/assets/js/app.js')?>"></script>
    </body>
</html>
