<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Main</li>
                <li>
                    <a href="/dashboard" class="waves-effect">
                        <span class="badge rounded-pill bg-primary float-end">20+</span>
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> <?=lang('Validation.menu_dashboard')?> </span>
                    </a>
                </li>
		        <li>
  		            <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ion ion-md-cash"></i>
                        <span><?=lang('Validation.users')?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="/users">
                                <span><?=lang('Validation.user')?></span>
                            </a>
                        </li>
                        <li>
                            <a href="/customers">
                                <span><?=lang('Validation.customer')?></span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->