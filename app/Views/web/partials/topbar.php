<!doctype html>
<html lang="th">
    <head>
        <meta charset="utf-8">
        <title>HR - <?= $pagetitle ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
        <meta content="Themesbrand" name="author">

        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css" rel="stylesheet" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url('assets/images/hr-favicon.ico')?>">
        <!-- Bootstrap Css -->
        <link href="<?= base_url('admin/assets/css/bootstrap.min.css') ?>" id="bootstrap-style" rel="stylesheet" type="text/css">
        <!-- Icons Css -->
        <link href="<?= base_url('admin/assets/css/icons.min.css') ?>" rel="stylesheet" type="text/css">
        <!-- App Css-->
        <link href="<?= base_url('admin/assets/css/app.min.css')?>" id="app-style" rel="stylesheet" type="text/css">
        <link href="<?= base_url('admin/assets/libs/alertify.js/css/alertify.css') ?>" rel="stylesheet" type="text/css">
    </head>
    <body data-sidebar="dark">
        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>
        <!-- Begin page -->
        <div id="layout-wrapper">
            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box">
                            <a href="/dashboard" class="logo logo-dark">
                                <span class="logo-sm">
                                    <?php if(session()->get('organization_logo') != "" && session()->get('organization_logo') != null){ ?>
                                        <img src="<?= session()->get('organization_logo') ?>" alt="" height="22">
                                    <?php }else{ ?>
                                        <img src="<?= AWS_URL.'assets/images/hr-logo.png'?>" alt="" height="22">
                                    <?php } ?>
                                </span>
                                <span class="logo-lg">
                                    <?php if(session()->get('organization_logo') != "" && session()->get('organization_logo') != null){ ?>
                                        <img src="<?= session()->get('organization_logo') ?>" alt="" height="17">
                                    <?php }else{ ?>
                                        <img src="<?= AWS_URL.'assets/images/hr-logo.png'?>" alt="" height="17">
                                    <?php } ?>
                                </span>
                            </a>

                            <a href="/dashboard" class="logo logo-light">
                                <span class="logo-sm">
                                    <?php if(session()->get('organization_logo') != "" && session()->get('organization_logo') != null){ ?>
                                        <img src="<?= session()->get('organization_logo') ?>" alt="" height="22">
                                    <?php }else{ ?>
                                        <img src="<?= AWS_URL.'assets/images/hr-logo-light.png'?>" alt="" height="22"> 
                                    <?php } ?>       
                                </span>
                                <span class="logo-lg">
                                    <?php if(session()->get('organization_logo') != "" && session()->get('organization_logo') != null){ ?>
                                        <img src="<?= session()->get('organization_logo') ?>" alt="" height="45">
                                    <?php }else{ ?>    
                                        <img src="<?= AWS_URL.'assets/images/hr-logo-light.png'?>" alt="" height="45">
                                    <?php } ?> 
                                </span>
                            </a>
                        </div>

                        <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                            <i class="mdi mdi-menu"></i>
                        </button>

                        <div class="d-none d-sm-block ms-2">
                            <h4 class="page-title"><?= $pagetitle ?></h4>
                        </div>
                    </div>

                    <!-- Search input -->
                    <div class="search-wrap" id="search-wrap">
                        <div class="search-bar">
                            <input class="search-input form-control" placeholder="Search">
                            <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                                <i class="mdi mdi-close-circle"></i>
                            </a>
                        </div>
                    </div>

                    <div class="d-flex">

                        <div class="dropdown d-none d-lg-inline-block me-2">
                            <button type="button" class="btn header-item toggle-search noti-icon waves-effect" data-target="#search-wrap">
                                <i class="mdi mdi-magnify"></i>
                            </button>
                        </div>

                        <div class="dropdown d-none d-lg-inline-block me-2">
                            <button type="button" class="btn header-item noti-icon waves-effect" data-bs-toggle="fullscreen">
                                <i class="mdi mdi-fullscreen"></i>
                            </button>
                        </div>

                        <div class="dropdown d-none d-md-block me-2">
                            <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php if(session()->get('locale') == "en"){ ?>
                                <span class="font-size-16"> English </span> <img class="ms-2" src="<?= AWS_URL.'assets/images/flags/us_flag.jpg'?>" alt="Header Language" height="16">
                            <?php }else{ ?> 
                                <span class="font-size-16"> ไทย </span> <img class="ms-2" src="<?= AWS_URL.'assets/images/flags/thai_flag.jpg'?>" alt="Header Language" height="16">
                            <?php } ?>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="/locale/en" class="dropdown-item notify-item">
                                    <img src="<?= AWS_URL.'assets/images/flags/us_flag.jpg'?>" alt="user-image" height="12"> <span class="align-middle"> English </span>
                                </a>
                                <a href="/locale/th" class="dropdown-item notify-item">
                                    <img src="<?= AWS_URL.'assets/images/flags/thai_flag.jpg'?>" alt="user-image" height="12"> <span class="align-middle"> ไทย </span>
                                </a>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block me-2">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="ion ion-md-notifications"></i>
                                <span class="badge bg-danger rounded-pill">3</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-notifications-dropdown">
                                <div class="p-3">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h5 class="m-0 font-size-16"> Notification (3) </h5>
                                        </div>
                                    </div>
                                </div>
                                <div data-simplebar style="max-height: 230px;">
                                    <a href="" class="text-reset notification-item">
                                        <div class="d-flex">
                                            <div class="avatar-xs me-3">
                                                <span class="avatar-title bg-success rounded-circle font-size-16">
                                                    <i class="mdi mdi-cart-outline"></i>
                                                </span>
                                            </div>
                                            <div class="flex-1">
                                                <h6 class="mt-0 font-size-15 mb-1">Your order is placed</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">Dummy text of the printing and typesetting industry.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="" class="text-reset notification-item">
                                        <div class="d-flex">
                                            <div class="avatar-xs me-3">
                                                <span class="avatar-title bg-warning rounded-circle font-size-16">
                                                    <i class="mdi mdi-message-text-outline"></i>
                                                </span>
                                            </div>
                                            <div class="flex-1">
                                                <h6 class="mt-0 font-size-15 mb-1">New Message received</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">You have 87 unread messages</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="" class="text-reset notification-item">
                                        <div class="d-flex">
                                            <div class="avatar-xs me-3">
                                                <span class="avatar-title bg-info rounded-circle font-size-16">
                                                    <i class="mdi mdi-glass-cocktail"></i>
                                                </span>
                                            </div>
                                            <div class="flex-1">
                                                <h6 class="mt-0 font-size-15 mb-1">Your item is shipped</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">It is a long established fact that a reader will</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div class="p-2 border-top">
                                    <div class="d-grid">
                                        <a class="btn btn-sm btn-link font-size-14  text-center" href="javascript:void(0)">
                                            View all
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php if(session()->get('profile_pic') != "" && session()->get('profile_pic') != null){ ?>
                                    <img class="rounded-circle header-profile-user" src="<?= session()->get('profile_pic')?>" alt="Header Avatar">
                                <?php }else{ ?>
                                    <img class="rounded-circle header-profile-user" src="<?= AWS_URL.'admin/assets/images/users/avatar-7.jpg' ?>" alt="Header Avatar">
                                <?php } ?>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <!-- item-->
                                <div class="p-3">
                                    <h5 class="m-0 font-size-16"><?= lang('Validation.hello')?> 
                                    <?php if(session()->get('locale') == "en")
                                          { 
                                            echo session()->get('first_name_en');
                                          }else{ 
                                            echo session()->get('first_name_th');
                                          }
                                    ?>
                                    </h5>
                                </div>
                                <a class="dropdown-item" href="/profile"><?= lang('Validation.profile')?></a>
                                <a class="dropdown-item d-block" href="#"><span class="badge bg-success float-end">11</span>Settings</a>
                                <a class="dropdown-item" href="#">Lock screen</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="/logout"><?= lang('Validation.logout')?></a>
                            </div>
                        </div>

                        <!-- <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                                <i class="mdi mdi-spin mdi-cog"></i>
                            </button>
                        </div> -->

                    </div>
                </div>
            </header>
        </div>
        <?= $this->include('web/partials/sidebar') ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Validation library file -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
       
        <script src="<?= base_url('admin/assets/libs/alertify.js/js/alertify.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/pages/alertify-init.js')?>"></script>

        <!--tinymce js-->
        <script src="<?= base_url('admin/assets/libs/tinymce/tinymce.min.js')?>"></script>

        <!-- init js -->
        <script src="<?= base_url('admin/assets/js/pages/form-editor.init.js')?>"></script>
