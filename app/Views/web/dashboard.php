<?= $this->include("web/partials/topbar") ?>
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4"><?= date("Y") ?> - <?= lang('Validation.dashboard')?> </h4>
                            <div class="table-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->include("web/partials/footer") ?>