<?php

namespace App\Models;

use CodeIgniter\Model;

class AccountsModel extends Model
{
    protected $table = 'accounts';
    protected $allowedFields = [
        'user_id',
        'name',
        'type',
        'token',
        'url',
        'custom_url',
        'logo',
        'app_hostname',
        'db_hostname',
        'db_name',
        'activated_at',
        'active',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}
