<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $table = 'users';
    protected $allowedFields = [
        'id',
        'username',
        'password',
        'membership_id',
        'user_level',
        'expire_on',
        'trail_used',
        'email',
        'firstname',
        'lastname',
        'address',
        'city',
        'country',
        'state',
        'zip',
        'token',
        'newsletter',
        'notes',
        'avatar',
        'active',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}
