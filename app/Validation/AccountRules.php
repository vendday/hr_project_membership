<?php

namespace App\Validation;

use Exception;

// use App\Models\UsersModel;

class AccountRules
{
    public function accountExist(string $str): bool
    {
        $users_model = model('App\Models\UsersModel');
        $users = $users_model->where('username', $str)->findAll();

        return count($users) === 0;
    }

    public function accountNameExist(string $str): bool
    {
        $account_model = model('App\Models\AccountsModel');
        $account = $account_model->where('name', $str)->findAll();

        return count($account) === 0;
    }

    public function callbackCreate(string $str, string $fields, array $data, &$error = null): bool
    {
        // random token
        helper('token');
        $register_token = unique_id();
        $account_name = strtolower(trim($data['account']));

        // new client schema name
        $schema_prefix = getenv('client.schemaPrefix');
        $schema_name = "{$schema_prefix}{$register_token}";
        // create master account user
        $user_data = [
            // 'username' => $username =  $this->request->getVar('email'),
            'username' => $username =  $data['email'],
            'email' => $username,
            // 'password' => $password_hashed = password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
            'password' => $password_hashed = password_hash($data['password'], PASSWORD_DEFAULT),
            'firstname' => '',
            'lastname' => '',
            'membership_id' => 1,
            'token' => $register_token,
            'active' => 'y',
            'created_by' => 1,
            'updated_by' => 1,
        ];
        // start create new account, transaction mode
        $db = \Config\Database::connect();

        // $db->transStart();
        $db->transBegin();

        $users_model = model('App\Models\UsersModel', true, $db);

        $user_id = $users_model->insert($user_data);

        $client_base_url = getenv('client.baseURL');

        // create master account
        $account_data = [
            'user_id' => $user_id,
            'name' => $account_name,
            'app_hostname' => $client_base_url,
            'db_hostname' => getenv('database.client.hostname'),
            'db_name' => $schema_name,
            'created_by' => 1,
            'membership_id' => 1,
            'token' => $register_token,
            'active' => 'n',
            'created_by' => 1,
            'updated_by' => 1,
        ];

        $accounts_model =  model('App\Models\AccountsModel', true, $db);

        $account_id = $accounts_model->insert($account_data);

        // $db->transComplete();
        // transaction rollback on fail
        if ($db->transStatus() === false) {
            $db->transRollback();
            $error = "Failed to crete membership account.";
            return false;
        }
        // create client database, call client app create membership
        $options = [
            'baseURI' => $client_base_url,
            'timeout'  => 30,
        ];

        $curl_result = false;

        try {
            $client = \Config\Services::curlrequest($options);

            $response = $client->request(
                'POST',
                'membership/create',
                [
                    'form_params' => [
                        'account_token' => $schema_name,
                        'account_username' => $username,
                        'account_password' => $password_hashed,
                    ],
                ]
            );
            // create membership response
            $curl_result = json_decode($response->getBody(), true);
        } catch (Exception $curl_exception) {
            $error = "Failed to create client account. [REQUEST ERROR]:" . $curl_exception->getMessage() . json_encode($client);
            $error .= "<br> [client]:" . json_encode($options);
            $db->transRollback();
            return false;
        }

        // transaction rollback on fail
        if ($curl_result['success'] === false) {
            $db->transRollback();
            $error = "Failed to create client account. [RESPONSE ERROR]:" . $curl_result['message'];
            return false;
        }

        // send activate email
        $activate_string = "{$register_token}{$account_name}";
        $activate_link = base_url("account-activate/$activate_string");

        if( ! ($sent_email = $this->_sendEmail($username, $activate_link) ) ){
            $db->transRollback();
            $error = "Failed to send email. [RESPONSE ERROR]:" . $sent_email;
            return false;
        }

        $db->transCommit();
        return true;
    }

    public function validateAccountToken(string $str)
    {
        $accounts_model = model('App\Models\AccountsModel');
        $input_token = trim($str, " ");
        $accounts = $accounts_model->where('token', $input_token)->findAll();

        if (count($accounts) !== 1) {
            return false;
        }

        return $accounts;
    }

    private function _sendEmail($to, $string)
    {
        // return true;
        $subject = "[24HR] Account Activation";
        $email = \Config\Services::email();
        // $to = 'donlachit.t@gmail.com';
        $email->setFrom(SMTP_USERNAME, SMTP_USERNAME);
        $email->setTo($to);
        // $email->setCC('another@another-example.com');
        // $email->setBCC('them@their-example.com');

        $email->setSubject($subject);
        $data = [
            'input' =>
            [
                $string
            ],
        ];

        $message = view('email/register_activation', $data);
        $email->setMessage($message);

        if (!$email->send(false)) {
            return $email->printDebugger();
        }

        return true;
    }
}
