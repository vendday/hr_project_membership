<?php

// Token Helpers

if (!function_exists('unique_id')) {
    /**
     * Get "unique_id"
     * from uniqid function based on micro time
     *
     * @param string $prefix
     * 
     * @param bool $more_entropy
     *
     * @throws Exception
     */
    function unique_id(string $prefix = "", bool  $more_entropy = false)
    {
        return uniqid($prefix, $more_entropy);
    }
}
