<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        return view('homepage/homepage');
    }

    public function login()
    {
        return view('auth/login');
    }

    public function resetPassword()
    {
        return view('auth/reset-password');
    }
    public function dashboard()
    {
        $pagetitle = lang('Validation.dashboard');
        
        return view('web/dashboard', ['pagetitle' => $pagetitle]);
    }
}
