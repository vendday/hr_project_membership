<?php

namespace App\Controllers;

// use CodeIgniter\Database\Config;
// use CodeIgniter\Database\Migration;
use CodeIgniter\API\ResponseTrait;
use Exception;

class Account extends \CodeIgniter\Controller
{
    use ResponseTrait;

    public function exist()
    {
        $response = array(
            'success' => false,
            'data' => null,
            'message' => null,
        );
        // POST submit
        if ($this->request->getMethod() === "post") {

            $token = $this->request->getVar('account_token');
            // setup validation
            // $validation =  \Config\Services::validation();
            // $validation->setRuleGroup('account');

            // validate inputs
            // fail
            // if (!$validation->withRequest($this->request)->run()) {
            if (($validated = $this->_validateAccountToken($token)) === false) {
                $response['message'] = "Account does not exists.";
                return $this->setResponseFormat('json')->respond($response);
            }

            if ($validated['active'] == 'n') {
                $response['message'] = "Please activate your account via email.";
                return $this->setResponseFormat('json')->respond($response);
            }

            $data = [
                'account_database' => $validated['db_name'],
                'account_token' => $validated['token'],
                'account_name' => $validated['name'],
            ];

            $response['data'] = $data;
            $response['success'] = true;
            return $this->setResponseFormat('json')->respond($response);
        }

        $response['message'] = "Invalid request.";
        return $this->setResponseFormat('json')->respond($response);
    }

    public function activate($token)
    {
        $response = array(
            'success' => false,
            'data' => null,
            'message' => null,
        );

        $request_token_length = strlen($token);
       
        if( $token == null || $request_token_length < 13 ) {
            return View('errors/html/error_404');
        }

        $request_account_token = substr($token, 0, 13);
        $request_account_name = substr($token, 13, strlen($token));
        // echo "$request_account_name  $request_account_token";

        $accounts_model = model('App\Models\AccountsModel');
        $accounts = $accounts_model->where('token', $request_account_token)->where('name', $request_account_name)->findAll();
        // dd($accounts);
        if( count($accounts) !== 1 ){
            return View('errors/html/error_404');
        }
        $account_update_data = [
            'active' => 'y',
            'activated_at' => date('Y-m-d H:i:s'),
        ];

        $accounts_model->update($accounts[0]['id'], $account_update_data);
        $view_data = [
            'input' => [10, getenv('client.baseURL')],
        ];
        return View('auth/activate-complete', $view_data);

        
    }

    private function _validateAccountToken(string $str)
    {
        $accounts_model = model('App\Models\AccountsModel');
        $input_token = trim($str, " ");
        $accounts = $accounts_model->where('token', $input_token)->orWhere('name', $input_token)->findAll();

        if (count($accounts) !== 1) {
            return false;
        }

        // if( $accounts[0]['active'] !== 'y' ){
        //     return false;
        // }


        return $accounts[0];
        // return [
        //     'account_database' => $accounts[0]['db_name'],
        //     'account_token' => $accounts[0]['token'],
        //     'account_name' => $accounts[0]['name'],
        // ];
    }
}
