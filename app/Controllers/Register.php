<?php

namespace App\Controllers;

use CodeIgniter\Database\Config;
// use CodeIgniter\Database\Migration;
use Exception;

class Register extends BaseController
{
    public function index()
    {
        // POST submit
        if ($this->request->getMethod() === "post") {

            // setup validation
            $validation =  \Config\Services::validation();
            // $validation->reset();
            $validation->setRuleGroup('signup');
            // validate inputs
            // fail
            if (!$validation->withRequest($this->request)->run()) {

                return view('auth/register', [
                    'validation' => $validation,
                ]);
            }
            // success
            // Setup account
            $validation->setRuleGroup('setup');
            if (!$validation->withRequest($this->request)->run()) {

                return view('auth/register', [
                    'validation' => $validation,
                ]);
            }
            // success
            // redirect to app url
            $login_url = getenv("client.baseURL");
            $view_data = [
                'input' => [
                    10, $login_url
                ]
                ];
            // header("Location: {$login_url}");
            // exit;
            return view('auth/register-complete', $view_data);
        }
        // GET request
        return view('auth/register');
    }
}
