<?php

/*
 |--------------------------------------------------------------------------
 | ERROR DISPLAY
 |--------------------------------------------------------------------------
 | In development, we want to show as many errors as possible to help
 | make sure they don't make it to production. And save us hours of
 | painful debugging.
 */
error_reporting(-1);
ini_set('display_errors', '1');

/*
 |--------------------------------------------------------------------------
 | DEBUG BACKTRACES
 |--------------------------------------------------------------------------
 | If true, this constant will tell the error screens to display debug
 | backtraces along with the other error information. If you would
 | prefer to not see this, set this value to false.
 */
defined('SHOW_DEBUG_BACKTRACE') || define('SHOW_DEBUG_BACKTRACE', true);

/*
 |--------------------------------------------------------------------------
 | DEBUG MODE
 |--------------------------------------------------------------------------
 | Debug mode is an experimental flag that can allow changes throughout
 | the system. This will control whether Kint is loaded, and a few other
 | items. It can always be used within your own application too.
 */
defined('CI_DEBUG') || define('CI_DEBUG', true);

defined('AWS_ACCESS_KEY_ID') || define('AWS_ACCESS_KEY_ID', getenv('AWS_ACCESS_KEY_ID'));
defined('AWS_SECRET_ACCESS_KEY') || define('AWS_SECRET_ACCESS_KEY', getenv('AWS_SECRET_ACCESS_KEY'));
defined('AWS_BUCKET') || define('AWS_BUCKET', getenv('AWS_BUCKET'));
defined('AWS_DEFAULT_REGION') || define('AWS_DEFAULT_REGION', getenv('AWS_DEFAULT_REGION'));
defined('AWS_URL') || define('AWS_URL', 'https://'.getenv('AWS_BUCKET').'.s3.'.getenv('AWS_DEFAULT_REGION').'.amazonaws.com/');

defined('SMTP_HOSTNAME') || define('SMTP_HOSTNAME', getenv('SMTP_HOSTNAME'));
defined('SMTP_PORT') || define('SMTP_PORT', getenv('SMTP_PORT'));
defined('SMTP_USERNAME') || define('SMTP_USERNAME', getenv('SMTP_USERNAME'));
defined('SMTP_PASSWORD') || define('SMTP_PASSWORD', getenv('SMTP_PASSWORD'));