<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;
use App\Validation\AccountRules;

class Validation
{
    //--------------------------------------------------------------------
    // Setup
    //--------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var string[]
     */
    public $ruleSets = [
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
        AccountRules::class,
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */
    public $templates = [
        'list'   => 'CodeIgniter\Validation\Views\list',
        'single' => 'CodeIgniter\Validation\Views\single',
        'my_list' => '_errors_list',
    ];

    //--------------------------------------------------------------------
    // Rules
    //--------------------------------------------------------------------

    // Signup Rule

    public $signup = [
        'account'        =>
        [
            'rules' => 'required|alpha_numeric_space|accountNameExist',
            'errors' => [
                'required'    => 'You must enter an account name.',
                'alpha_numeric_space' => 'Only alphabet, number and space.',
                'accountNameExist' => 'This name has already been taken. Please try another name.',
            ]

        ],
        'email'        =>
        [
            'rules' => 'required|valid_email|accountExist',
            'errors' => [
                'required'    => 'You must enter an email.',
                'valid_email' => 'Please check the Email field. It does not appear to be valid.',
                'accountExist' => 'This email has already registered. Please try another email.',
            ]

        ],
        'password'     =>
        [
            'rules' => 'required|min_length[8]',
            'errors' => [
                'required'    => 'You must enter a password.',
                'min_length'  => 'Your {field} is too short.',
            ]
        ],
        'password_confirm' =>
        [
            // 'rules' => 'required|matches[password]|callbackCreate[email,password]',
            'rules' => 'required|matches[password]',
            'errors' => [
                'required'    => 'You must confirm a password.',
                'matches'  => 'Your Password does not match',
                'createCallback'  => 'createCallback',
            ]
        ],

    ];

    // Account setup

    public $setup = [
        'setup'        =>
        [
            'rules' => 'callbackCreate[email,account,password, password_confirm]',
            'errors' => [
                'callbackCreate' => 'Fail to register account. Please try again later',
            ]

        ],
    ];

    // Account Rule

    public $account = [
        'account_token'        =>
        [
            'rules' => 'required|validateAccountToken',
            'errors' => [
                'required'    => 'Missing request parameters.',
                'accountExist' => 'This email has already registered. Please try another email.',
            ]

        ],
    ];
}
