<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SystemUsersSeeder extends Seeder
{
    public function run()
    {
        $model = model('UsersModel');

		$model->insert(
			[
				'username' => 'admin',
				'email' => 'team@vendday.com',
				'password' => password_hash('P@ssw0rd',PASSWORD_DEFAULT),
                'firstname' => 'system',
                'lastname' => 'admin',
			]
		);
    }
}
