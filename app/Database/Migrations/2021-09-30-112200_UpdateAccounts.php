<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UpdateAccounts extends Migration
{
    public function up()
    {
        $forge = \Config\Database::forge();
        $fields = [
            'token' => [
                'type' => 'VARCHAR',
                'constraint'     => 23,
                'null' => false,
                'after'=>'type'
            ],
        ];
        $forge->addColumn('accounts', $fields);
    }

    public function down()
    {
        //
    }
}
