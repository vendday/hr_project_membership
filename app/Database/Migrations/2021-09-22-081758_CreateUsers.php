<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUsers extends Migration
{
    protected $DBGroup = 'default';

    protected $TableName = 'users';

    public function up()
    {
        $this->db->disableForeignKeyChecks();

        $this->forge->addField(
            [
                'id'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'unsigned'       => true,
                    'auto_increment' => true,
                ],
                'username'       => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                ],
                'password' => [
                    'type' => 'TEXT',
                    'null' => true,
                ],
                'membership_id'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                ],
                'user_level'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                    'default'       => 0,
                ],
                'expire_on'          => [
                    'type'           => 'datetime',
                ],
                'trail_used'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                    'default'       => 0
                ],
                'email'          => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                ],
                'firstname'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 50,
                    'null' => true,
                ],
                'lastname'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 50,
                    'null' => true,
                ],
                'address'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 150,
                    'null' => true,
                ],
                'city'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 100,
                    'null' => true,
                ],
                'country'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 4,
                ],
                'state'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 100,
                    'null' => true,
                ],
                'zip'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 20,
                    'null' => true,
                ],
                'token'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                ],
                'newsletter'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                    'default'       => 0,
                ],
                'notes'          => [
                    'type'           => 'TEXT',
                    'null' => true,
                ],
                'avatar'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 150,
                    'null' => true,
                ],
                'active'          => [
                    'type'           => "enum('y','n','t','b')",
                ],

                'created_by'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'default'     => 0,
                  
                ],
                'updated_by'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'default'     => 0,
                  
                ],
                'created_at datetime default current_timestamp',
                'updated_at datetime default current_timestamp on update current_timestamp',
            ]
        );

        $this->forge->addKey('id', true);

        $this->forge->createTable($this->TableName);

        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        //
        $this->forge->dropTable($this->TableName);
    }
}
