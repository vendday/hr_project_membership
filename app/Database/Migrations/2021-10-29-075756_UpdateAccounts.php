<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UpdateAccounts extends Migration
{
    public function up()
    {
        $forge = \Config\Database::forge();
        $fields = [
            'activated_at' => [
                'type' => 'DATETIME',
                'default' => null,
                'after' => 'db_name'
            ],
        ];
        $forge->addColumn('accounts', $fields);
    }

    public function down()
    {
        //
    }
}
