<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateMemberships extends Migration
{
    protected $DBGroup = 'default';

    protected $TableName = 'memberships';

    public function up()
    {
        $this->db->disableForeignKeyChecks();

        $this->forge->addField(
            [
                'id'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'unsigned'       => true,
                    'auto_increment' => true,
                ],
                'title'       => [
                    'type'       => 'VARCHAR',
                    'constraint' => '100',
                ],
                'description' => [
                    'type' => 'TEXT',
                    'null' => true,
                ],
                'package_group'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                ],
                'price'          => [
                    'type'           => 'DECIMAL(10,2)',
                ],
                'days'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                ],
                'period'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 1,
                ],
                'plan_perks'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 500,
                ],
                'trial'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                ],
                'recurring'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                ],
                'private'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                ],
                'active'          => [
                    'type'           => 'TINYINT',
                    'constraint'     => 1,
                ],
            ]
        );

        $this->forge->addKey('id', true);

        $this->forge->createTable($this->TableName);

        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        //
        $this->forge->dropTable($this->TableName);
    }
}
