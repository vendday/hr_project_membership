<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UpdateAccounts extends Migration
{
    public function up()
    {
        $forge = \Config\Database::forge();
        $fields = [
            'active' => [
                'type' => 'VARCHAR',
                'constraint' => 1,
                // y = yes, n = no, t = terminate
                'default' => 'n',
                'after' => 'activated_at'
            ],
        ];
        $forge->addColumn('accounts', $fields);
    }

    public function down()
    {
        //
    }
}
