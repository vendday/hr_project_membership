<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateAccounts extends Migration
{
    protected $DBGroup = 'default';

    protected $TableName = 'accounts';

    public function up()
    {
        $this->db->disableForeignKeyChecks();

        $this->forge->addField(
            [
                'id'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'unsigned'       => true,
                    'auto_increment' => true,
                ],

                'user_id'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                ],

                'name'       => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                    'null' => true,
                ],
                'type' => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'default' => 1,
                ],
                'url'          => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                    'null' => true,
                ],
                'custom_url'          => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                    'null' => true,
                ],
                'logo'          => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                    'null' => true,
                ],
                'app_hostname'          => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                    'null' => true,
                ],
                'db_hostname'          => [
                    'type'       => 'VARCHAR',
                    'constraint' => '50',
                    'null' => true,
                ],
                'db_name'          => [
                    'type'           => 'VARCHAR',
                    'constraint'     => 50,
                    'null' => true,
                ],

                'created_by'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'default'     => 1,

                ],
                'updated_by'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'default'     => NULL,
                ],
                'created_at datetime default current_timestamp',
                'updated_at datetime default current_timestamp on update current_timestamp',
            ]
        );

        $this->forge->addKey('id', true);

        $this->forge->createTable($this->TableName);

        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        //
        $this->forge->dropTable($this->TableName);
    }
}
