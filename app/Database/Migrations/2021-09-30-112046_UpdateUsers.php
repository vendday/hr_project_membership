<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UpdateUsers extends Migration
{
    public function up()
    {
        $forge = \Config\Database::forge();
        $fields = [
            'token' => [
                'type' => 'VARCHAR',
                'constraint'     => 23,
                'null' => false,
            ],
        ];
        $forge->modifyColumn('users', $fields);
    }

    public function down()
    {
        //
    }
}
